#include <iostream>
#include "funciones.h"
#include <iomanip>
using namespace std;

void prueba() {
	cout << "hola mundo!" << endl;
}

void cargarVentas(float m[][31]) {
	int pais, dia, venta;

	cout << endl << "Ingresar el numero de pais" << endl;
	cin >> pais;

	while (pais >= 0) {

		cout << endl << "Ingresar dia de la venta" << endl;
		cin >> dia;
		cout << endl << "Ingresar el monto de la venta de ese dia" << endl;
		cin >> venta;
		m[pais - 1][dia - 1] += venta;



		cout << endl << "Ingresar el numero de pais" << endl;
		cin >> pais;
	}

	cout << endl << "=================================================================" << endl;
	cout << endl << "====================== FIN DE LA CARGA ==========================" << endl;
	cout << endl << "=================================================================" << endl;
	cout << endl;
	cout << endl;
}

//void reporteDeVentas(float m[][31]) {
//	int i, j;
//	for (i = 0; i < 3; i++) {
//		cout << endl << "=================================================================" << endl;
//		cout << endl << "================= Ventas del pais nro " << i + 1 << " =========================" << endl;
//		cout << endl << "=================================================================" << endl;
//		cout << endl;
//		cout << endl << "\tDIA\tVALOR ACUMULADO";
//		cout << endl << "\t-----------------------";
//		for (j = 0; j < 31; j++) {
//			if (m[i][j] != 0) {
//				cout << endl << "\t " << j + 1 << "\t    $" << m[i][j] << endl;
//			}
//		}
//	}
//}

void reporteDeVentas(float m[][31]) {
	int i, j;
	for (i = 0; i < 3; i++) {
		cout << endl << "=================================================================" << endl;
		cout << endl << "================= Ventas del pais nro " << i + 1 << " =========================" << endl;
		cout << endl << "=================================================================" << endl;
		cout << endl;
	
		cout << left;
		cout << setw(5) << "DIA |";
		cout << setw(12) << " VALOR ACUM" << endl;
		cout << "---------------" << endl;

		for (j = 0; j < 31; j++) {
			if (m[i][j] != 0) {
				//cout << endl << "\t " << j + 1 << "\t    $" << m[i][j] << endl;
				cout << left;
				cout << setw(5) << j + 1;
				cout << setw(12) << m[i][j];
				cout << endl;
				

			}
		}
	}
}


void reporteDiasSinVentas(float m[][31]) {
	int i, j, cantDiasSinVentas;
	for (i = 0; i < 3; i++) {
		cantDiasSinVentas = 0;
		cout << endl;
		cout << endl;
		cout << endl << "=================================================================" << endl;
		cout << endl << "============= Dias sin Ventas del pais nro " << i + 1 << " ====================" << endl;
		cout << endl << "=================================================================" << endl;
		cout << endl;
		cout << endl << "DIAS SIN VENTAS";
		cout << endl << "---------------";
		for (j = 0; j < 31; j++) {
			if (m[i][j] == 0) {
				cantDiasSinVentas++;
			}
		}
		cout << endl << "Dias sin ventas: " << cantDiasSinVentas << endl;
	}
}